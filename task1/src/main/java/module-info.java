module c195.task1 {
    requires javafx.controls;
    requires javafx.fxml;

    opens c195.task1 to javafx.fxml;
    exports c195.task1;
}
